<?php

namespace App\Http\Controllers;

use App\Models\Collection;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\CollectionRequest;
use RoobieBoobieee\Translatables\Models\Translation;

class CollectionController extends Controller
{
    public function list(): View
    {
        $collections = Collection::all();

        return view('collections.list', compact('collections'));
    }

    public function add(): View
    {
        $collections = Collection::dictionary('name', 'uuid');

        return view('collections.add', compact('collections'));
    }

    public function save(CollectionRequest $request): RedirectResponse
    {
        $collection              = new Collection();
        $collection->name        = Translation::make($request->get('name'));
        $collection->description = Translation::make($request->get('description'));

        if ($request->get('parent')) {
            $parent = Collection::whereUuid($request->get('parent'))->firstOrFail();

            $collection->parent()->associate($parent);
        }

        $collection->save();

        return redirect()->route('dashboard')
            ->with('status', __('Collection created!'));
    }
}
