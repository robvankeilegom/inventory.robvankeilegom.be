<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Http\Requests\ItemRequest;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use RoobieBoobieee\Translatables\Models\Translation;

class ItemController extends Controller
{
    public function list(): View
    {
        $items = Item::all();

        return view('items.list', compact('items'));
    }

    public function add(): View
    {
        return view('items.add');
    }

    public function save(ItemRequest $request): RedirectResponse
    {
        $item              = new Item();
        $item->name        = Translation::make($request->get('name'));
        $item->description = Translation::make($request->get('description'));

        $item->save();

        return redirect()->route('dashboard')
            ->with('status', __('Item created!'));
    }
}
