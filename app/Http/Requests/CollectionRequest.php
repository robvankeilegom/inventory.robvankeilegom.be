<?php

namespace App\Http\Requests;

use App\Models\Collection;
use App\Rules\EfficientUuid;
use App\Rules\EfficientUuidExists;
use Illuminate\Foundation\Http\FormRequest;
use RoobieBoobieee\Translatables\Rules\Translatable;

class CollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                new Translatable(),
            ],
            'description' => [
                new Translatable(),
            ],
            'parent' => [
                'nullable',
                new EfficientUuid(),
                new EfficientUuidExists((new Collection())->getTable()),
            ],
        ];
    }
}
