@props([ 'disabled' => false, 'value' => [] ])


<table class="w-full">
@foreach(config('translatables.accepted_locales', []) as $locale)
    <tr>
        <td>
            <x-label name="{{ $attributes->get('name')}}[{{$locale}}]" :value="$locale" />
        </td>
        <td class="py-1">
            <input
                {{ $disabled ? 'disabled' : '' }}
                name="{{ $attributes->get('name')}}[{{$locale}}]"
                id="{{ $attributes->get('name')}}[{{$locale}}]"
                value="{{ $value[$locale] ?? '' }}"
                {!! $attributes->merge([
                    'class' => 'rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 ml-2'
                ]) !!}
            />
        </td>
    </tr>
@endforeach
</table>
