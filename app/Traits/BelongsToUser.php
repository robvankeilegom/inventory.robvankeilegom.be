<?php

namespace App\Traits;

use App\Models\User;
use App\Scopes\ScopedByUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongsToUser
{
    public static function bootBelongsToUser(): void
    {
        static::addGlobalScope(new ScopedByUser());
    }

    public function initializeBelongsToUser(): void
    {
        $this->hidden[] = $this->getOwnerIdColumn();

        self::creating(function ($model) {
            if (! \Auth::check()) {
                throw new \RuntimeException('No user logged in');
            }

            $model->user()->associate(Auth::user());
        });
    }

    /**
     * Get the name of the "user id" column. Defaults to owner_id.
     */
    public function getOwnerIdColumn(): string
    {
        // @phpstan-ignore-next-line
        return defined('static::OWNER_ID') ? static::OWNER_ID : 'owner_id';
    }

    /**
     * Get the fully qualified "owner_id" column.
     */
    public function getQualifiedOwnerIdColumn(): string
    {
        return $this->qualifyColumn($this->getOwnerIdColumn());
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, $this->getOwnerIdColumn());
    }
}
