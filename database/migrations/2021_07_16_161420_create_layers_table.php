<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('layers', function (Blueprint $table) {
            $table->id();
            $table->efficientUuid('uuid');

            $table->foreignId('owner_id')->constrained('users');

            $table->json('data');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('layers_translations', function (Blueprint $table) {
            $table->translations('layers');

            $table->string('name');
            $table->longText('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('layers_translations');
        Schema::dropIfExists('layers');
    }
}
