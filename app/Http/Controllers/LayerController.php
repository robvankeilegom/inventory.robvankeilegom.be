<?php

namespace App\Http\Controllers;

use App\Models\Layer;
use App\Http\Requests\LayerRequest;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use RoobieBoobieee\Translatables\Models\Translation;

class LayerController extends Controller
{
    public function list(): View
    {
        $layers = Layer::all();

        return view('layers.list', compact('layers'));
    }

    public function add(): View
    {
        $layers = Layer::dictionary('name', 'uuid');

        return view('layers.add', compact('layers'));
    }

    public function data(Layer $layer): View
    {
        return view('layers.data', compact('layer'));
    }

    public function save(LayerRequest $request): RedirectResponse
    {
        $layer              = new Layer();
        $layer->name        = Translation::make($request->get('name'));
        $layer->description = Translation::make($request->get('description'));
        $layer->data        = [];

        $layer->save();

        return redirect()->route('dashboard')
            ->with('status', __('Layer created!'));
    }
}
